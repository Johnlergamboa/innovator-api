require('dotenv').config();
import {initialize_db} from './initialize/db';
import { initialize_express } from './initialize/express';




const boot = async () => {
    initialize_db()
    initialize_express()
}


boot()
import express from 'express'
export const InnovatorRoutes = express.Router();
import { InnovatorControl } from '../controls/InnovatorController'
const controls = new InnovatorControl

import multer from 'multer';

var storage = multer.diskStorage({
  destination:function (req,file,cb){
    cb(null,'uploads/')
  },
  filename:function(req,file,cb){
    cb(null,Date.now() + '.jpg')
  }
})

const upload = multer({storage:storage})

InnovatorRoutes.get('/innovator', controls.test)

InnovatorRoutes.post('/innovator/jobDescriptionAI', controls.jobDescriptionAI)

InnovatorRoutes.post('/innovator/file', upload.single('image'),controls.insertImage);



import express, { Request, Response } from "express";


export const CompanyRoutes = express.Router();

// get
CompanyRoutes.get('/company', (req:Request, res: Response) => {
    res.send("GET - Company")
})


// post
CompanyRoutes.post('/company', (req:Request, res: Response) => {
    res.send("POST - Company")
})


// put
CompanyRoutes.put('/company', (req:Request, res: Response) => {
    res.send("PUT - Company")
})

// delete
CompanyRoutes.delete('/company', (req:Request, res: Response) => {
    res.send("DELETE - Company")
})



import { Request, Response } from "express"
import OpenAI from 'openai';

function promiseTest() {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve('RESOLVED')
        }, 3000)
    })
}

export class InnovatorControl {
    async test(req: Request, res: Response) {
        const result = await promiseTest()
        return res.send(`USER: ${result}`)
    }

    async jobDescriptionAI(req: Request, res: Response) {
        const {
            job_title,
            industry
        } = req.body;
        const openai = new OpenAI({
            apiKey: process.env.CHATGPT_API_KEY, // defaults to process.env["OPENAI_API_KEY"]
        });

        const completion = await openai.chat.completions.create({
            messages: [{ role: 'assistant', content: `Give me a job posting description with responsibilites about ${job_title} position for ${industry} industry. Format into object with the property of job_posting_description and job_responsibilites` }],
            model: 'gpt-3.5-turbo',
        });
        res.json(completion.choices)
    }

    async insertImage(req:Request,res:Response) {
        const { file, body } = req
        // const data = JSON.parse(body.data)
        try {
            return res.json({image: file?.path});

        } catch (error:any) {
            console.log("%c Line:41 🍪 error", "color:#33a5ff", error);
          
        }
      }
    
}


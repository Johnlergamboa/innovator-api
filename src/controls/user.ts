import { Request, Response } from "express"


function promiseTest() {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve('RESOLVED')
        }, 3000)
    })
}

export class UserControl {
    async fetchAllUser(req:Request, res:Response) {
        const result = await promiseTest()
        return res.send(`USER: ${result}`)
    }
}


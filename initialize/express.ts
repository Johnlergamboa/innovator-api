
import express from 'express'
const { SERVER_PORT } = process.env
const app = express()



import { UserRoutes } from '../src/routes/user';
import { InnovatorRoutes } from '../src/routes/innovator';


export async function initialize_express() {
    app.use('/uploads', express.static('uploads/'))
    app.use(express.json())
	app.use(UserRoutes)
    app.use(InnovatorRoutes)
    app.listen(SERVER_PORT, () => { console.log(`Server running on: http://localhost:${SERVER_PORT}`) })
}












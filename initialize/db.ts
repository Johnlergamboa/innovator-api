const { MongoClient } = require('mongodb');

const {DB_URL, DB_NAME} = process.env;
const client = new MongoClient(DB_URL);


export async function initialize_db() {
    try {
        await client.connect();
        console.log('Connected successfully to server');

    } catch(e: any) {
        console.log("DB ERROR: ", e.message)
    }
}



